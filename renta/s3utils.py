from storages.backends.s3boto import S3BotoStorage


def StaticS3BotoStorage():
    return S3BotoStorage(location="renta/static")


def MediaS3BotoStorage():
    return S3BotoStorage(location="renta/media")
