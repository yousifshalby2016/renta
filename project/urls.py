from django.urls import include, path
from rest_framework import routers
from project.views import (
    ContactUsCreateView,
    LoginView,
    AreaViewSet,
    PriceRangeViewSet,
    PropertyTypeViewSet,
    ContactInfoAPI,
    AboutUsListingAPIView,
    UserContractViewSet,
    PropertyViewSet,
    ForgetPasswordViewSet,
    FooterImageViewSet,
    TermsAndConditionViewSet,
    SocialMediaInfoAPI,
    AmentiesiewSet
)

router = routers.DefaultRouter()
router.register("area", AreaViewSet, basename="area")
router.register("price-range", PriceRangeViewSet, basename="price-range")
router.register("property-type", PropertyTypeViewSet, basename="property-type")
router.register("footer-image", FooterImageViewSet, basename="footer-image")
router.register("amenties", AmentiesiewSet, basename="amenties")
router.register(
    "terms-conditions", TermsAndConditionViewSet, basename="terms-conditions"
)
router.register("my-unit", UserContractViewSet, basename="user-unit")
router.register("property", PropertyViewSet, basename="property")
router.register("users/password", ForgetPasswordViewSet, basename="password_reset")


urlpatterns = [
    path("", include(router.urls)),
    path("users/login", LoginView.as_view(), name="login"),
    path("contact-us/", ContactUsCreateView.as_view(), name="contact-us"),
    path("contact-us-info/", ContactInfoAPI.as_view(), name="contact-us-info"),
    path("social-media/", SocialMediaInfoAPI.as_view(), name="social-media"),
    path("about-us/", AboutUsListingAPIView.as_view(), name="about-us"),
]
