from rest_framework import serializers
from project.models import (
    Amenties,
    Area,
    ContactUs,
    ContactInfo,
    AboutUs,
    FooterImages,
    PriceRange,
    PropertyImages,
    SocialMediaInfo,
    TermsAndCondition,
    UserContract,
    User,
    PropertyType,
    Properties,
)
from django.utils.translation import gettext as _
from rest_framework.exceptions import ValidationError
from project.services import CustomModelSerializer
from rest_framework.serializers import CurrentUserDefault, HiddenField


class ContactUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactUs
        fields = "__all__"


class ContactInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactInfo
        fields = ["address", "email", "contact_no", "find_us"]


class SocialMediaInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialMediaInfo
        fields = "__all__"


class AboutUsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AboutUs
        fields = "__all__"


class PriceRangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceRange
        fields = "__all__"


class ImageFooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = FooterImages
        fields = "__all__"


class PropertyTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PropertyType
        fields = "__all__"


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = "__all__"


class TermsAndConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TermsAndCondition
        fields = "__all__"


class AmentiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amenties
        fields = "__all__"


class TokenSerializer(serializers.Serializer):
    access_token = serializers.CharField(max_length=200)
    refresh_token = serializers.CharField(max_length=200)


class CustomAuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(
        trim_whitespace=False,
    )

    def validate(self, attrs):
        password = attrs.get("password", None)
        email = attrs.get("email")
        user = self._login_by_login_phone(email, password)
        attrs["user"] = user
        return attrs

    def _login_by_login_phone(self, email, password):
        user = User.objects.filter(email=email).first()

        if user is None:
            self._raise_login_validation_error()

        if user and not user.check_password(password):
            self._raise_login_validation_error()

        return user

    def _raise_login_validation_error(self):
        msg = {"details": [_("Unable to log in with provided credentials.")]}
        raise serializers.ValidationError(msg, code="authorization")


class UserSerializer(CustomModelSerializer):
    password = serializers.CharField(write_only=True, min_length=6)

    class Meta:
        model = User
        fields = (
            "id",
            "phone",
            "password",
            "email",
            "gender",
            "date_of_birth",
            "first_name",
            "last_name",
            "address",
        )

    def validate(self, validated_data):
        if self.context["request"].method == "POST":
            if "email" not in validated_data:
                raise ValidationError(_("you must provide email to signUp"))

            if User.objects.filter(
                email=validated_data["email"], is_active=True
            ).exists():
                raise serializers.ValidationError(_("Email already exist"))

        return validated_data


class PropertiesSerializer(serializers.ModelSerializer):
    area = serializers.CharField(source="area.name", read_only=True, required=False)
    property_type = serializers.CharField(
        source="property_type.name", read_only=True, required=False
    )
    is_rented_property = serializers.BooleanField(read_only=True, default=False)

    class Meta:
        model = Properties
        fields = "__all__"
        read_only_fields = (
            "property_type",
            "area",
        )

    def to_representation(self, obj):
        data = super(PropertiesSerializer, self).to_representation(obj)
        if obj.id is not None:
            data.update(
                {
                    "property_image": PropertyImages.objects.filter(
                        property_id=obj.id
                    ).values_list("image")[:4]
                }
            )
        return data


class PropertiesDetailSerializer(PropertiesSerializer):
    def to_representation(self, obj):
        data = super(PropertiesSerializer, self).to_representation(obj)
        if obj.id is not None:
            data.update(
                {
                    "property_image": PropertyImages.objects.filter(
                        property_id=obj.id
                    ).values_list("image")
                }
            )
        return data


class UserContractSerializer(serializers.ModelSerializer):
    user = HiddenField(default=CurrentUserDefault())
    area = PropertiesSerializer(many=True, read_only=True)
    property_type = PropertyTypeSerializer(many=True, read_only=True)
    property_id = PropertiesSerializer(read_only=True)

    class Meta:
        model = UserContract
        fields = "__all__"


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=200)


class CodeMailSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=200)
    code = serializers.CharField(
        allow_blank=False,
        max_length=30,
        trim_whitespace=True,
        min_length=4,
    )


class PasswordCodeMailSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=200)
    code = serializers.CharField(
        allow_blank=False,
        max_length=30,
        trim_whitespace=True,
        min_length=4,
    )
    password = serializers.CharField(
        allow_blank=False,
        min_length=6,
        trim_whitespace=True,
    )
    re_enter_password = serializers.CharField(
        allow_blank=False,
        min_length=6,
        trim_whitespace=True,
    )
