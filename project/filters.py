import django_filters
from django.db.models import Q

from project.models import Properties


class PropertiesFilter(django_filters.FilterSet):
    price_gte = django_filters.NumberFilter(field_name="price", lookup_expr="gte")
    price_lte = django_filters.NumberFilter(field_name="price", lookup_expr="lte")
    area_gte = django_filters.NumberFilter(field_name="squared_area", lookup_expr="gte")
    area_lte = django_filters.NumberFilter(field_name="squared_area", lookup_expr="lte")
    number_of_rooms = django_filters.NumberFilter(method="rooms_filter")
    most_viewed = django_filters.BooleanFilter(method='most_viewed_filter')
    
    class Meta:
        model = Properties
        fields = (
           'number_of_rooms','Bathrooms', 'price', 'property_type__id', 'area__id', 'squared_area',
           'most_viewed'
        )

    def rooms_filter(self, queryset, name, value):
        if value > 6:
            return queryset.filter(Bedrooms__gt=6)
        return queryset.filter(Bedrooms=value)
    
    def most_viewed_filter(self, queryset, name, value):
        return queryset.filter(most_viewed_units=True)