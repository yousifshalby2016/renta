from django.db import models
from django.db.models import Exists, OuterRef


class PropertyManager(models.QuerySet):
    def is_rented_property(self):
        from project.models import UserContract

        return self.annotate(
            is_rented_property=Exists(
                UserContract.objects.filter(property_id=OuterRef("id"))
            )
        )
