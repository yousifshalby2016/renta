# Generated by Django 2.2.16 on 2023-03-11 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0009_auto_20230311_2303'),
    ]

    operations = [
        migrations.AddField(
            model_name='termsandcondition',
            name='privacy_policy_ar',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='termsandcondition',
            name='privacy_policy_en',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='termsandcondition',
            name='terms_conditions_ar',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='termsandcondition',
            name='terms_conditions_en',
            field=models.TextField(null=True),
        ),
    ]
