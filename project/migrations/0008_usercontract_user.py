# Generated by Django 2.2.16 on 2023-03-11 18:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0007_auto_20230308_2139'),
    ]

    operations = [
        migrations.AddField(
            model_name='usercontract',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_contract', to=settings.AUTH_USER_MODEL),
        ),
    ]
